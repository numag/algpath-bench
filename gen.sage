import itertools
import json
from pathlib import Path
from sage.all import *
import random as rdm

class Name:
    name = None

    def get_name(self):
        pass


class DegreesCtx(Name):
    degrees = None

    def __init__(self, degrees=None, default="factorized"):
        if degrees is None:
            try:
                self.degrees = list(map(int, input(
                    f"Input a comma separated list of degrees, e.g. 3, 7, 2. Bad/no input will be considered as default={default}").replace(" ", "").split(",")))
            except:
                self.degrees = default
            self.name = Path(str(self.degrees).replace("[",
                                                       "").replace("]", "").replace(", ", "-"))
        else:
            self.degrees = degrees


class StructureCtx(Name):
    structure = None

    def __init__(self, structure=None, default="factorized"):
        if structure is None:
            try:
                self.structure = input(
                    f"Input a structure (developped, factorized). Bad/no input will be considered as default={default}")
            except:
                self.structure = default
            self.name = Path(self.structure)
        else:
            self.structure = structure


class IntegerCtx(Name):
    value = None

    def __init__(self, value=None, default=0, message="Input an integer."):
        if value is None:
            try:
                self.value = int(input(
                    f"{message} Bad/no input will be considered as default={default}"))
            except:
                self.value = default
            self.name = Path(str(self.value))
        else:
            self.value = value


class System:
    ring = None
    system = None

    def get_degrees(self):
        return [f.degree() for f in self.system]


class StartSystem(System):
    fiber = None

    def compute_fiber(self):
        pass


class Problem:
    system = None
    variables = None
    parameters = None
    path = None
    fiber = None

    def to_json(self, target_path=Path("./data")):
        index = 1
        (target_path / self.name).mkdir(parents=True, exist_ok=True)
        while (target_path / self.name / f"{str(index)}.json").exists():
            index += 1

        print("Writting on test data file ...")
        data_file = (target_path / self.name / f"{str(index)}.json").open("w")
        json.dump({"system": self.system,
                   "variables": self.variables,
                   "parameters": self.parameters,
                   "path": self.path,
                   "fiber": self.fiber},
                  data_file,
                  indent=2)
        data_file.close()


class DenseCtx(Name, System):
    degrees_ctx = None

    def __init__(self, degrees=None):
        self.degrees_ctx = DegreesCtx(degrees=degrees)
        self.ring = PolynomialRing(
            CC, names=[f"x_{i}" for i in range(len(self.degrees_ctx.degrees))])
        dictlist = [((1 + sum(self.ring.gens())) ^ di).dict()
                    for di in self.degrees_ctx.degrees]
        for dict in dictlist:
            for k in dict:
                dict[k] = normalvariate(0, 1) + I*normalvariate(0, 1)
        self.system = [self.ring(dict) for dict in dictlist]
        self.name = Path(
            "dense" + f"-{self.degrees_ctx.name}"*(degrees is None))

    def get_degrees(self):
        return self.degrees_ctx.degrees


class SumPowLinCtx(Name, System):
    structure_ctx = None
    n_ctx = None
    d_ctx = None
    l_ctx = None

    def get_degrees(self):
        return [self.d_ctx.value for _ in range(self.n_ctx.value)]

    def __init__(self, n=None, d=None, l=None, structure=None):
        self.structure_ctx = StructureCtx(structure=structure)
        self.n_ctx = IntegerCtx(n, default=3, message="Dimension ?")
        self.d_ctx = IntegerCtx(d, default=3, message="Degree ?")
        self.l_ctx = IntegerCtx(l, default=3, message="Length ?")
        self.ring = PolynomialRing(
            CC, names=[f"x_{i}" for i in range(self.n_ctx.value)])
        self.system = [f"{str(choice([-1, 1]))} + " + " + ".join(["(" + str(sum([normalvariate(-1, 1)*v for v in self.ring.gens()])
                                                                            ) + f")^{self.d_ctx.value}" for _ in range(self.l_ctx.value)]) for _ in range(self.n_ctx.value)]
        print(self.system)
        if self.structure_ctx.structure == "developped":
            self.system = list(map(self.ring, self.system))
        self.name = Path("sumpowlin" + f"-{self.structure_ctx.name}"*(
            structure is None) + f"-{self.n_ctx.name}-{self.d_ctx.name}-{self.l_ctx.name}")


class ResultantsCtx(Name, System):
    n_ctx: None
    d_ctx: None
    degrees: None
    structure_ctx = StructureCtx("factorized")

    def get_degrees(self):
        return self.degrees

    def __init__(self, n=None, d=None):
        self.n_ctx = IntegerCtx(n, default=3, message="Dimension ?")
        self.d_ctx = IntegerCtx(d, default=3, message="Degree ?")

        dense_ctx = DenseCtx(
            [self.d_ctx.value for _ in range(self.n_ctx.value)])
        self.ring = dense_ctx.ring
        variables = list(map(str, self.ring.gens()))
        self.system = dense_ctx.system

        assert "y" not in variables
        n = self.n_ctx.value
        d = self.d_ctx.value
        
        Cpy = PolynomialRing(CC, names=["y"] + variables)
        f = Cpy.random_element(degree=self.d_ctx.value, terms=binomial(n + 1 + d, d))
        g = Cpy.random_element(degree=self.d_ctx.value, terms=binomial(n + 1 + d, d))
        h = g.resultant(f, Cpy.gens()[0])
        h = h/h.coefficients()[0]
        self.system[0] = self.ring(h)
        self.degrees = [f.degree() for f in self.system]
        self.name = Path(
            "resultants" + f"-{self.n_ctx.name}-{self.d_ctx.name}")


class GradientCtx(Name, System):
    nvars_ctx: None
    d_ctx: None
    structure_ctx = StructureCtx("factorized")

    def __init__(self, nvars=None, d=None, l=None):
        self.nvars_ctx = IntegerCtx(
            nvars, default=3, message="Number of variables for the polynomial?")
        self.d_ctx = IntegerCtx(nvars, default=3, message="Degree?")
        n = self.nvars_ctx.value
        self.ring = PolynomialRing(CC, names=[f"x_{i}" for i in range(n)])
        coeffs = [[normalvariate(-1, 1) for _ in range(n)] for _ in range(n)]
        self.system = [" + ".join([f"{self.d_ctx.value}*({coeffs[i][k]})*(" + str(sum([coeffs[i][j]*v for j, v in enumerate(self.ring.gens())])
                                                                                  ) + f")^{self.d_ctx.value - 1}" for i in range(n)]) for k in range(n)]
        self.name = Path(
            f"gradient_{self.nvars_ctx.value}-{self.d_ctx.value}")

    def get_degrees(self):
        return [self.d_ctx.value - 1 for _ in range(self.nvars_ctx.value)]
    
class SPLGradient(Name, System):
    n_ctx: None
    d_ctx: None
    l_ctx: None
    structure_ctx = StructureCtx("factorized")

    def __init__(self, n=None, d=None, l=None):
        self.n_ctx = IntegerCtx(n, default=3, message="Dimension ?")
        self.d_ctx = IntegerCtx(d, default=3, message="Degree ?")
        self.l_ctx = IntegerCtx(l, default=3, message="Length ?")
        self.ring = PolynomialRing(
            CC, names=[f"x_{i}" for i in range(self.n_ctx.value)])
        
        n, d, l = self.n_ctx.value, self.d_ctx.value, self.l_ctx.value
        c = [CC.random_element() for _ in range(l)]
        a = [[CC.random_element() for _ in range(n)] for _ in range(l)]
        λ = [" + ".join([f"({aij})*{xj}" for aij, xj in zip(ai, self.ring.gens())]) for ai in a]
        self.system = [" + ".join([f"{d} * ({a[i][k]}) * ({c[i]} + {λ[i]})^{d - 1}" for i in range(l)]) for k in range(n)]
        self.name = Path("spl-gradient" + f"-{self.n_ctx.name}-{self.d_ctx.name}-{self.l_ctx.name}")

    def get_degrees(self):
        return [self.d_ctx.value - 1 for _ in range(self.n_ctx.value)]

class KatsuraCtx(Name, System):
    n_ctx: None

    def __init__(self, n=None):
        self.n_ctx = IntegerCtx(
            n, default=5, message="Size of katsura system?")
        n = self.n_ctx.value
        self.ring = PolynomialRing(
            CC, names=[f"x_{i}" for i in range(n + 1)])
        self.system = sage.rings.ideal.Katsura(
            self.ring, n + 1).gens()
        self.name = Path(f"katsura_{n}")

    def get_degrees(self):
        return super().get_degrees()


class URootsCtx(Name, System):
    degrees_ctx = None
    fiber = None
    fiber_type = "random"
    fiber_size = 100
    fiber_name = ""
    compute_fiber_bool = False

    def __init__(self, degrees=None, compute_fiber=None):
        self.degrees_ctx = DegreesCtx(degrees=degrees)
        print(self.get_degrees())
        self.fiber_init(compute_fiber=compute_fiber)
        self.ring = PolynomialRing(
            CC, names=[f"x_{i}" for i in range(len(self.degrees_ctx.degrees))])
        self.system = [f"({normalvariate(0, 1)} + I*{normalvariate(0, 1)})*({v}^{d} - 1)" for v,
                       d in zip(self.ring.gens(), self.degrees_ctx.degrees)]
        self.name = Path("uroots" + f"-{self.degrees_ctx.name}"*(
            degrees is None) + f"-{self.fiber_name}"*self.compute_fiber_bool)

    def get_degrees(self):
        return self.degrees_ctx.degrees

    def fiber_init(self, compute_fiber=None):
        if compute_fiber is None:
            try:
                self.compute_fiber_bool = bool(int(input(
                    "Should the fiber be computed ? (Y/N)").replace("Y", "1").replace("y", "1").replace("N", "0").replace("n", "0")))
            except:
                pass
        else:
            self.compute_fiber_bool = compute_fiber

        if self.compute_fiber_bool:
            try:
                self.fiber_type = input(
                    "Which type of fiber? (all, random)")
            except:
                pass

            self.fiber_name = self.fiber_type
            if self.fiber_type == "random":
                try:
                    self.fiber_size = int(input("Fiber size? (int)"))
                except:
                    pass
                self.fiber_name += f"-{self.fiber_size}"

            self.compute_fiber()

    def compute_fiber(self):
        exps = [[str(CC(exp(k*2*CC.gen()*pi/di)))
                 for k in range(di)] for di in self.get_degrees()]

        if self.fiber_type == "all":
            self.fiber = list(itertools.product(*exps))
        else:
            self.fiber = [[choice(exp) for exp in exps]
                          for _ in range(self.fiber_size)]


class TargetCtx(Name):
    target_type = None
    target = None

    def __init__(self, target_type=None, default="dense", message="Input a kind of system."):
        if target_type is None:
            try:
                self.target_type = input(
                    f"{message} (dense, uroots, sumpowlin, resultants, gradient, katsura, spl-gradient) Bad/no input will be considered as default={default}")
            except:
                self.target_type = default
            self.name = Path(str(self.target_type))
        else:
            self.target_type = target_type

        if self.target_type == "uroots":
            self.target = URootsCtx(compute_fiber=False)
        elif self.target_type == "sumpowlin":
            self.target = SumPowLinCtx()
        elif self.target_type == "resultants":
            self.target = ResultantsCtx()
        elif self.target_type == "gradient":
            self.target = GradientCtx()
        elif self.target_type == "katsura":
            self.target = KatsuraCtx()
        elif self.target_type == "spl-gradient":
            self.target = SPLGradient()
        else:
            self.target = DenseCtx()


class LinearHomotopyCtx(Name, Problem):
    target_ctx = None
    start = None

    def __init__(self, structure=None, target_type=None):
        self.target_ctx = TargetCtx(target_type=target_type, default="dense")
        self.start = URootsCtx(
            degrees=self.target_ctx.target.get_degrees(), compute_fiber=True)
        self.name = "linear" / self.target_ctx.target.name / \
            Path(str(self.start.name).replace("uroots-", ""))

        # compute homotopy
        try:
            structure = self.target_ctx.target.structure_ctx.structure
        except:
            structure = ""

        if structure == "factorized":
            self.system = [f"t*({ft}) + (1 - t)*({fs})" for ft,
                           fs in zip(self.target_ctx.target.system, self.start.system)]
        else:
            CPt = PolynomialRing(
                CC, names=['t'] + [str(v) for v in self.target_ctx.target.ring.gens()])
            self.system = [str(CPt.gens()[0]*CPt(ft) + (1 - CPt.gens()[0])*CPt(fs))
                           for ft, fs in zip(self.target_ctx.target.system, self.start.system)]

        self.variables = [str(v) for v in self.target_ctx.target.ring.gens()]
        self.parameters = ["t"]
        self.path = [["0.0"], ["1.0"]]
        self.fiber = self.start.fiber  # TODO: convert to strings

    def to_json(self, target_path=Path("./data")):
        return super().to_json(target_path)


class NewtonHomotopyCtx(Name, Problem):
    target_ctx = None

    def __init__(self, target_type=None, start_type=None):
        self.target_ctx = TargetCtx(target_type=target_type, default="dense")
        self.name = "newton" / self.target_ctx.target.name

        # homotopy
        n = len(self.target_ctx.target.system)
        z = vector(CC, [normalvariate(0, 1) + I*normalvariate(0, 1)
                   for _ in range(n)])
        fiber = [list(z/z.norm())]
        Cpt = PolynomialRing(
            CC, names=["t"] + list(map(str, self.target_ctx.target.ring.gens())))

        D = {}
        for z, v in zip(fiber[0], self.target_ctx.target.ring.gens()):
            D[str(v)] = z

        T_fiber = [sage_eval(str(f), D) for f in self.target_ctx.target.system]

        try:
            structure = self.target_ctx.target.structure_ctx.structure
        except:
            structure = ""
        if structure == "factorized":
            self.system = [f"{f} - (1 - t)*({c})" for f,
                           c in zip(self.target_ctx.target.system, T_fiber)]
        else:
            self.system = [str(Cpt(f) - (1 - Cpt.gens()[0])*Cpt(c))
                           for f, c in zip(self.target_ctx.target.system, T_fiber)]

        fiber = [[str(zi) for zi in z] for z in fiber]

        self.variables = list(map(str, self.target_ctx.target.ring.gens()))
        self.parameters = ["t"]
        self.path = [["0.0"], ["1.0"]]
        self.fiber = fiber  # TODO: convert to strings


class Problem:
    system = None
    variables = None
    parameters = None
    path = None
    fiber = None

    def to_json(self, target_path=Path("./data")):
        index = 1
        (target_path / self.name).mkdir(parents=True, exist_ok=True)
        while (target_path / self.name / f"{str(index)}.json").exists():
            index += 1

        print("Writting on test data file ...")
        print(self.name)
        data_file = (target_path / self.name / f"{str(index)}.json").open("w")
        json.dump({"system": self.system,
                   "variables": self.variables,
                   "parameters": self.parameters,
                   "path": self.path,
                   "fiber": self.fiber},
                  data_file,
                  indent=2)
        data_file.close()


class ProblemCtx(Name):
    problem_type = None
    problem = None

    def __init__(self, problem_type=None, default="linear", message="Input a kind of problem."):
        if problem_type is None:
            try:
                self.problem_type = input(
                    f"{message} (linear, newton) Bad/no input will be considered as default={default}")
            except:
                self.problem_type = default
            self.name = Path(str(self.problem_type))
        else:
            self.problem_type = problem_type

        if self.problem_type == "newton":
            self.problem = NewtonHomotopyCtx()
        else:
            self.problem = LinearHomotopyCtx()

        self.name = self.problem.name

    def to_json(self):
        self.problem.to_json()


problem = ProblemCtx()
problem.to_json()
