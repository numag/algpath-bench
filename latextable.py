import json
import argparse
import os
import re

parser = argparse.ArgumentParser(description = "A python script to generate a latex output out of a table of results.")
parser.add_argument("table", help="A JSON table file (should be generated via synthetize.py)")
args = parser.parse_args()

assert os.path.exists(args.table), f"{args.table} not found"

table_file = open(args.table, "r")
table = json.load(table_file)

syn = {
    "failures": "fail.",
    "medsteps": "med.",
    "minsteps": "min.",
    "maxsteps": "max.",
    "meantime": "time/path",
    "bezout": "\# zeros",
    "paths": "\# paths",
    "dimension": "dim.",
    "medmean hc algpath": "?",
    "timepersteps": "time/steps",
    "stepspersec": "ksteps/s ",
    "tottime": "time (s)",
    "medstepsratio": "med.",
    "max. deg.": "deg.",
    "f": "$f$",
    "df": "$\mathrm{d}f$",
    "instructions" : "circuit size",
    "homotopycontinuation": "HomotopyContinuation.jl",
    "macaulay2": "Macaulay2",
    "sirocco": "SIROCCO"
}

def title(D, key):
    if key in D:
        return D[key]
    else:
        return key

# Latex output
cols_len = len(table["header"][-1])
print(r"\setlength{\tabcolsep}{2pt}\begin{tabular*}{\textwidth}{l@{\extracolsep{\fill}}" 
    + "".join(["r" for _ in range(cols_len - 1)])
    + "}")
print(r"\toprule")


for i, line in enumerate(table["header"]):
    cell_list = []
    cmidrule_list = []
    cols = 1
    for cell in line:
        cell_width = cell["w"]
        cell_str = title(syn, cell["str"])
        if cell_str == "":
            cmidrule_list.append("")
            cell_list.append("")
        else:
            cmidrule_list.append(f"\\cmidrule(lr){{{cols}-{cols + cell_width - 1}}}")
            cell_list.append(f"\\multicolumn{{{cell_width}}}{{c}}{{{cell_str}}}")
        cols += cell_width

    print(" & ".join(cell_list) + " \\\\")
    if i != len(table["header"]) - 1:
        print(" ".join(cmidrule_list))

print(r"\midrule")

for line in table["table"]:
    cell_list = []
    for cell in line:
        s = str(cell["str"])
        w = cell["w"]
        if w == 1:
            cell_list.append(s)
        else:
            cell_list.append(f"\\multicolumn{{{w}}}{{c}}{{{s}}}")
    print(" & ".join(cell_list) + " \\\\")

print(r"\bottomrule")
print(r"\end{tabular*}")


# def fusion(LT1, LT2):
#     if LT1 == []:
#         return LT2
#     elif LT2 == []:
#         return LT1
#     else:
#         m = len(LT1)
#         n = len(LT2)
#         LT1 += [
#             [{"str": "", "width": node["width"]} for node in LT1[-1]]
#             for _ in range(n - m)
#         ]
#         LT2 += [
#             [{"str": "", "width": node["width"]} for node in LT2[-1]]
#             for _ in range(m - n)
#         ]
#         return [l1 + l2 for l1, l2 in zip(LT1, LT2)]


# def _latex_header(s, header):
#     res = []
#     if type(header) is dict:
#         for item in header.items():
#             _latex_header(*item)
#             res = fusion(res, _latex_header(*item))
#         if res == []:
#             total_width = 1
#         else:
#             total_width = len(res[0])
#         res.append([{"str": s, "width": total_width}])
#     return res


# def latex_header(header):
#     res = _latex_header("", header)[:-1]
#     res.reverse()
#     header_lines = []
#     for i, layer in enumerate(res):
#         line = []
#         cmidrule = []
#         cols = 1
#         for key in layer:
#             key_width = key["width"]
#             key_str = title(syn, key["str"])
#             if key_width == 1:
#                 cmidrule.append("")
#                 line.append(key_str)
#             else:
#                 cmidrule.append(f"\\cmidrule(lr){{{cols}-{cols + key_width - 1}}}")
#                 line.append(f"\\multicolumn{{{key_width}}}{{c}}{{{key_str}}}")
#             cols += key_width

#         header_lines.append(" & ".join(line) + " \\\\")
#         if i != len(res) - 1:
#             header_lines.append(" ".join(cmidrule))

#     return header_lines, len(res[-1])


# def get_leaves(D):
#     L = []
#     if type(D) is dict:
#         for v in D.values():
#             L += get_leaves(v)
#     else:
#         L = [str(D)]
#     return L


# header_list, cols_len = latex_header(table["header"])
# out_list = [
#      r"\setlength{\tabcolsep}{2pt}\begin{tabular*}{\textwidth}{l@{\extracolsep{\fill}}" 
#     + "".join(["r" for _ in range(cols_len - 1)])
#     + "}",
#     "\\toprule",
# ]
# out_list += header_list

# n_local = len(get_leaves(table[table["data_list"][0]][table["pkg_list"][0]]))

# out_list.append("\\midrule")
# for data in table["data_list"]:
#     line_str = (
#         re.sub(
#             " & ".join(["(timeout \w* \w*)" for _ in range(n_local)]),
#             "\\\\multicolumn{" + str(n_local) + "}{c}{" + "\\1" + "}",
#             " & ".join(get_leaves(table[data])) + " \\\\",
#         )
#         .replace("timeout", "$>$")
#         .replace("(1.0)", "")
#     )
#     out_list.append(line_str)

# out_list += ["\\bottomrule", "\\end{tabular*}"]
# print("\n".join(out_list))
