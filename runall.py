import argparse
import subprocess
import os
import glob

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--perf", action = 'store_true', default = False, help = "perf stat the benchmark and put the result in perflog.txt")
parser.add_argument("-t", "--timeout", nargs = "?", const = 300, help = "To run the benchmark with a timeout. Should be written as <number><unit> where <unit> may be nothing (this implicitely means seconds), s, m or h, e.g. 1h")
parser.add_argument("-m", "--mem", nargs = "?", const = 8, help = "Maximum amount of memory used for the different tests. Should be written as <number><unit> where <unit> may be nothing, K, M or G, e.g. 100M")
parser.add_argument("-n", action = 'store_true', help = "To only run benchmarks not previously done")
parser.add_argument("-P", "--packages", nargs = "?", const = ",".join([os.path.basename(os.path.splitext(path)[0]) for path in glob.glob("packages/*.py")]), default = ",".join([os.path.basename(os.path.splitext(path)[0]) for path in glob.glob("packages/*.py")]), help = "comma separated list of packages to test, e.g. <pkg1>,<pkg2>,<pkg3>. The possible packages are algpath, macaulay2, sirocco and homotopycontinuation. Default : %(default)s")
args = parser.parse_args()

packages = args.packages.split(',')
data = glob.glob("data/*.json")

for i, pkg_name in enumerate(packages):
    print(f"Package {i + 1}/{len(packages)}")
    for j, data_path in enumerate(data):
        print(f"Data {j + 1}/{len(data)}")
        pkg_path = f"packages/{pkg_name}.py"
        data_name = os.path.splitext(os.path.basename(data_path))[0]
        if not(os.path.exists(f"benchmarks/{data_name}/{pkg_name}") and args.n):
            subprocess.run(["python3", "runtest.py", pkg_path, data_path] + ["--timeout", str(args.timeout)]*(args.timeout != None) + ["--mem", str(args.mem)]*(args.mem != None) + ["--perf"]*args.perf)
