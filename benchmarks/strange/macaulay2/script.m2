

loadPackage "NumericalAlgebraicGeometry";
loadPackage "JSON";
R = CC[x];
S = {x + 0.0};
T = {x + 1.0};
solsS = {{0.0}};
stats = timing(x = track(S, T, solsS, tStepMin => 0.00000000000000001, Projectivize => true, Predictor => Certified, Normalize => true));
fail_cpt = 0;
steps = {};
for z in x do(
    if status z == MinStepFailure then (fail_cpt = fail_cpt + 1 ; steps = append(steps, null))
    else steps = append(steps, z.cache.NumberOfSteps - 1);
)
<< toJSON hashTable{"time" => toString stats#0, "steplist" => steps, "failures" => fail_cpt};
exit();

