Running as unit: run-r6baf25741876437dbba7a76f9f9e7042.scope; invocation ID: c90aa1220a974ad3a186036553eb328e
The latest version of Julia in the `release` channel is 1.10.2+0.x64.linux.gnu. You currently have `1.10.0+0.x64.linux.gnu` installed. Run:

  juliaup update

in your terminal shell to install Julia 1.10.2+0.x64.linux.gnu and update the `release` channel to that version.
