import argparse
import subprocess
import os
import glob
import json
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument(
    "entries", help="The entry files that specifies what should be benchmarked")
parser.add_argument("-p", "--perf", action='store_true', default=False,
                    help="perf stat the benchmark and put the result in perflog.txt")
parser.add_argument("-t", "--timeout", nargs="?", const=300,
                    help="To run the benchmark with a timeout. Should be written as <number><unit> where <unit> may be nothing (this implicitely means seconds), s, m or h, e.g. 1h")
parser.add_argument("-m", "--mem", nargs="?", const=8,
                    help="Maximum amount of memory used for the different tests. Should be written as <number><unit> where <unit> may be nothing, K, M or G, e.g. 100M")
parser.add_argument("-n", action='store_true',
                    help="To only run benchmarks not previously done")
args = parser.parse_args()

# Recovering data
assert Path(args.entries).exists(), "Specify valid path"

entries_file = open(args.entries, "r")
try:
    entries = json.load(entries_file)
    data_list = entries["data_list"]
    pkg_list = entries["packages"]
except:
    entries_file.close()
    print("Wrong file format")
    exit(1)

for i, pkg_name in enumerate(pkg_list):
    print(f"Package {i + 1}/{len(pkg_list)}")
    for j, data_path in enumerate(data_list):
        print(f"Data {j + 1}/{len(data_list)}")
        pkg_path = (Path("packages") / Path(f"{pkg_name}")).with_suffix(".py")
        data_name = Path(data_path)
        try:
            assert args.n
            info_file = open(str(Path("benchmarks") /
                                 data_name / pkg_name / "info.json"), "r")
            info = json.load(info_file)
            assert not info["killed manually"]
            assert not info["script error"]
        except:
            subprocess.run(["python3", "runtest.py", str(pkg_path), str(("data" / Path(data_path)).with_suffix(".json"))] + ["--timeout", str(args.timeout)]*(
                args.timeout != None) + ["--mem", str(args.mem)]*(args.mem != None) + ["--perf"]*args.perf)
