import argparse
from pathlib import Path
import os
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("file", help="A file containing data test")
parser.add_argument(
    "algpath_path", help="A algpath binary that supports --arithmetic arb-reckless")
args = parser.parse_args()

# Build adaptive if not already built
algpath_bin_path = Path(args.algpath_path) / "target" / "release" / "algpath"
if not algpath_bin_path.exists():
    current_dir = os.getcwd()
    print("Building algpath adaptive...")
    print("Moving to " + str(Path(args.algpath_path).with_suffix("")))
    os.chdir(Path(args.algpath_path).with_suffix(""))
    try:
        _, _ = subprocess.Popen(["cargo", "build", "--release"]).communicate()
    except:
        print("Algpath build failure")
        exit(1)

    print("Moving back to " + str(current_dir))
    os.chdir(current_dir)

assert algpath_bin_path.exists()

command = open("command.sh", "w")
command.write(f"""#!/bin/sh
              
{algpath_bin_path} {args.file} --diff backward --homogenize --arithmetic arb-reckless --jobs 1""")
command.close()
