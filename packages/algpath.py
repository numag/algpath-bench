import argparse

parser = argparse.ArgumentParser()
parser.add_argument("file", help = "A file containing data test")
parser.add_argument("algpath_path", help = "A algpath branch main binary")
args = parser.parse_args()

command = open("command.sh", "w")
command.write(f"""#!/bin/sh
              
{args.algpath_path} {args.file} --homogenize --diff backward --jobs 1""")
command.close()