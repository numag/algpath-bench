using JSON
using HomotopyContinuation
using Statistics

println(stderr, "Parsing file ...")
pb = JSON.parsefile(ARGS[1])

println(stderr, "Warmup system ...")
eval(Meta.parse("@var " * join(pb["variables"], " ")))
eval(Meta.parse("@var " * join(pb["parameters"], " ")))

I = im

warmup_sys = System([eval(Meta.parse(v)) - sum([eval(Meta.parse(p)) for p in pb["parameters"]]) for v in pb["variables"]], parameters = [eval(Meta.parse(p)) for p in pb["parameters"]])
res = solve(warmup_sys, [0.0 for _ in pb["variables"]]; compile = false, threading = false, start_parameters = [0.0 for _ in pb["parameters"]], target_parameters = [1.0 for _ in pb["parameters"]], show_progress = false)

println(stderr, "Initializing system ...")
sys = System([eval(Meta.parse(eq)) for eq in pb["system"]], parameters = [eval(Meta.parse(p)) for p in pb["parameters"]]);
start_solutions = [[ComplexF64(eval(Meta.parse(s))) for s in sol] for sol in pb["fiber"]];
p0 = ComplexF64[eval(Meta.parse(start)) for start in pb["path"][1]]
p1 = ComplexF64[eval(Meta.parse(start)) for start in pb["path"][2]]

println(stderr, "Solving 1/2...")
stats_overhead = @timed (res = solve(sys, start_solutions; compile = false, threading = false, start_parameters = p0, target_parameters = p1, show_progress = false))
println(stderr, "Solving 2/2...")
stats = @timed (res = solve(sys, start_solutions; compile = false, threading = false, start_parameters = p0, target_parameters = p1, show_progress = false))

println(stderr, "Certifying ...")
certificate = certify(sys, res, p1, show_progress = false)

println(stderr, "Writting to output ...")
output = Dict{String, Any}()
output["time"] = stats.time
output["overheadtime"] = stats_overhead.time
output["failures"] = nfailed(res)
output["steplist"] = [steps(path) for path in path_results(res)]
# output["extendpreclist"] = [path.extended_precision_used for path in path_results(res)] 
output["ncertified"] = ndistinct_certified(certificate) 
JSON.print(stdout, output)