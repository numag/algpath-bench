import json
import argparse

# Parsing input arguments
parser = argparse.ArgumentParser()
parser.add_argument("file", help = "A file containing data test")
parser.add_argument("m2_path", help = "A m2 binary path")
args = parser.parse_args()

# Recovering the data from the json file given by arguments
data_file = open(args.file)
data = json.load(data_file)
data_file.close()

vars_data = data["variables"]
vars_parameter = data["parameters"]
T = data["system"].copy()
S = data["system"].copy()
for v, vs, ve in zip(vars_parameter, data["path"][0], data["path"][1]):
    S = [f.replace(v, vs) for f in S]
    T = [f.replace(v, ve) for f in T]

# String manipulation for the input to correspond with M2 syntax
vars_str = str(vars_data).replace("'", "")
T_str = str(T).replace("'", "").replace("[", "{").replace("]", "}").replace("I", "ii")
S_str = str(S).replace("'", "").replace("[", "{").replace("]", "}").replace("I", "ii")
solsS_str = str(data["fiber"]).replace("'", "").replace("[", "{").replace("]", "}").replace("I", "ii")

script = open("script.m2", "w")
script.write("""

loadPackage "NumericalAlgebraicGeometry";
loadPackage "JSON";
R = CC%s;
S = %s;
T = %s;
solsS = %s;
stats = timing(x = track(S, T, solsS, tStepMin => 0.00000000000000001, Projectivize => true, Predictor => Certified, Normalize => true));
fail_cpt = 0;
steps = {};
for z in x do(
    if status z == MinStepFailure then (fail_cpt = fail_cpt + 1 ; steps = append(steps, null))
    else steps = append(steps, z.cache.NumberOfSteps - 1);
)
<< toJSON hashTable{"time" => toString stats#0, "steplist" => steps, "failures" => fail_cpt};
exit();

""" % (vars_str, S_str, T_str, solsS_str))
script.close()

command = open("command.sh", "w")
command.write(f"""#!/bin/sh

{args.m2_path} script.m2 --silent
""")
command.close()
