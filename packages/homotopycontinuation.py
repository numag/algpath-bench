import argparse
import os
import sys

parser = argparse.ArgumentParser()
parser.add_argument("file", help = "A file containing data test")
parser.add_argument("julia_path", help = "A julia binary path")
args = parser.parse_args()

command = open("command.sh", "w")
command.write(f"""#!/bin/sh
              
{args.julia_path} {os.path.dirname(sys.argv[0])}/_homotopycontinuation.jl {args.file}""")
command.close()