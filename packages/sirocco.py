import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("file", help = "A file containing data test")
parser.add_argument("sage_path", help = "A sage binary path")
args = parser.parse_args()

data_file = open(args.file)
data = json.load(data_file)
data_file.close()

vars_data = data["variables"]
vars_parameter = data["parameters"]

script = open("script.sage", "w")
script.write("""

import sage.schemes.curves.zariski_vankampen as zvk
import time
import sys

QQi = i.parent()
vars = %s + %s
CPt = PolynomialRing(CC, names = vars)
QQiPt = PolynomialRing(QQi, names = vars)
f = CPt(%s[0])

coeffs = f.dict()             
for key in coeffs.keys():
    z = coeffs[key]
    coeffs[key] = QQ(z.real()) + i*QQ(z.imag())
f = QQiPt(coeffs)

initial_sols = [[CC(zi) for zi in z] for z in %s]
total_steps = 0

start = time.time()

steplist = []
for i, z in enumerate(initial_sols):
    print("Continuation " + str(i + 1) + "/" + str(len(initial_sols)), file = sys.stderr)
    zeta = zvk.followstrand(f, [], %s, %s, z[0])
    steplist.append(len(zeta) - 1)
             
print("{\\"time\\" : \\"" + str(time.time() - start) + "\\", \\"steplist\\" : " + str(steplist) + "}", end = "")
             
""" % (vars_parameter, vars_data, data["system"], data["fiber"], float(data["path"][0][0]), float(data["path"][1][0])))

command = open("command.sh", "w")
command.write(f"""#!/bin/sh

{args.sage_path} script.sage""")
command.close()